FROM gcr.io/kaniko-project/executor:debug


LABEL org.opencontainers.image.authors="p.stekunov@profinansy.net"


RUN wget https://github.com/a8m/envsubst/releases/download/v1.4.2/envsubst-Linux-x86_64 -O /kaniko/envsubst && \
    chmod a+x /kaniko/envsubst



